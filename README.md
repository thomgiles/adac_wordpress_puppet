# wordpress

#### Table of Contents

1. [Description]
2. [Setup]
3. [Usage]
4. [Requirements]
5. [Development]

## Description

This puppet repository installs wordpress on a University of Nottingham
CentOS server.

## Setup

This module requires puppet-agent to be installed on the client VM and the
client VM connected to the server.

To do this you need to run the following:

### ON THE VM:

```console
sudo rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm

sudo yum -y install puppet-agent

sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

find /etc/puppetlabs/puppet/ssl -name *`hostname`* -exec rm {} \;

sudo /opt/puppetlabs/bin/puppet agent --server=psvfw.nottingham.ac.uk --waitforcert=240 --test`
```


### ON THE PUPPET SERVER:

Sign the certificate:

```console
sudo /opt/puppetlabs/bin/puppet cert sign --all
```

The VM must then be defined in the puppet-master manifest sheet:

```console
node 'CLIENTNAME' {
class { 'wordpress': }
 apache::vhost { 'CLIENTIP':
 port => '80', 
 docroot => '/var/www/html'
 }
}
```
 
## Usage

This method of using wordpress uses the puppet-labs apache module to control  
the apache server settings. 

This means that you can specify ssl keys and certs directly in the manifest on 
the puppet master:

```console
apache::vhost { 'cert.example.com':
  port     => '443',
  docroot  => '/var/www/cert',
  ssl      => true,
  ssl_cert => '/etc/ssl/fourth.example.com.cert',
  ssl_key  => '/etc/ssl/fourth.example.com.key',
  ssl_chain  => '/etc/ssl/fourth.example.com.intermediate',
  ssl_protocol => '-SSLv2', '-SSLv3',
  ssl_cipher => 'HIGH:MEDIUM:!aNULL:!MD5',
}
```

There are many other apache settings avalible from this puppet interface
for more information on the puppet-forge apache module please visit:

https://forge.puppet.com/puppetlabs/apache/1.4.1

## Requirements

This module depends on the puppet-forge apache and mysql modules. You shouldn't
need to download them if you are deploying this code from the master but this 
information is useful for debugging and future modifications. 

## Development

I have plans to improve several aspects of this plugin in the future but right
now there is no agreed roadmap for development



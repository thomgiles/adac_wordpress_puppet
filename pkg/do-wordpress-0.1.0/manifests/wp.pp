class wordpress::wp {

    # Copy the Wordpress bundle to /tmp
    exec { 'retrieve_wordpress':
       cwd => "/tmp",
       command => '/usr/bin/wget https://wordpress.org/latest.tar.gz',
    }

    # Extract the Wordpress bundle
    exec { 'extract':
        cwd => "/tmp",
        command => "tar -xvzf latest.tar.gz",
        path => ['/bin'],
    }

    # Copy to /var/www/html
    exec { 'copy':
        command => "cp -r /tmp/wordpress/* /var/www/html",
        require => Exec['extract'],
        path => ['/bin'],
    }

    # Generate the wp-config.php file using the template
    file { '/var/www/html/wp-config.php':
        ensure => present,
        require => Exec['copy'],
        content => template("wordpress/wp-config.php.erb")
    }
}

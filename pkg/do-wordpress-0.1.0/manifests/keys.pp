 # set ownership of directory

class wordpress::keys {
  exec { 'create SSH key':
     command => '/bin/ssh-keygen -t rsa -b 4096 -f /home/apache/wp_rsa -q -N ""',
  }

  exec { 'set folder permissions':
     command => '/bin/find /var/www/html -type d -exec chmod 755 {} \;',
  }

  exec { 'set file permissions':
     command => '/bin/find /var/www/html -type f -exec chmod 644 {} \;',
  }

  exec { 'set theme permissions':
     command => '/bin/chmod -R g+w /var/www/html/wp-content/themes',
  }

  exec { 'set plugin permissions':
     command => '/bin/chmod -R g+w /var/www/html/wp-content/plugins',
  }

  exec { 'set content permissions':
     command => '/bin/chmod g+w /var/www/html/wp-content',
  }

  exec { 'set directory ownership':
     command => '/bin/chown -R apache:apache /var/www/html/*',
  }

  file { "/home/apache/wp_rsa":
    owner => "apache",
    ensure => file,
    group => "apache",
    mode => "0640",
  }

  file { "/home/apache/wp_rsa.pub":
    owner => "apache",
    ensure => file,
    group => "apache",
    mode => "0640",
  }

  file { "/home/apache/.ssh":
    owner => "apache",
    ensure => directory,
    recurse => true,
    group => "apache",
    mode => "0700",
  }

  exec { 'copy SSH key':
     command => '/bin/cp /home/apache/wp_rsa.pub /home/apache/.ssh/authorized_keys',
  }

   file { "/home/apache/.ssh/authorized_keys":
    owner => "apache",
    ensure => file,
    group => "apache",
    mode => "0644",
  }

  exec { 'restart httpd':
     command => '/bin/systemctl restart  httpd.service',
  }
}

